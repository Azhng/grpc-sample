# gRPC basic example: 

## To run server: 

``` bash
cd /path/to/build.sbt
sbt compile
sbt run
```

## To run client:

``` bash
cd /path/to/python-client/

# compile protobuf
python -m grpc_tools.protoc -I../src/main/protobuf --python_out=. --grpc_python_out=. ../src/main/protobuf/hello.proto

# run client 
python client.py
```